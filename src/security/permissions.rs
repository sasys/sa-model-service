//Copyright SoftwareByNumbers Ltd 2021

//! Provides operations that integrate the security configuration, the security cache and the
//! security adaptor into a more abstract security go/no go function.

use std::sync::{Mutex, Arc};

use lru::LruCache;
use log::{info, trace, error, warn};

use super::security_adaptor::{Token, Providers, Client, get_client, ProviderError};
use super::config::{Provider, find_mapped_permissions, check_permissions, Permission};
use super::cache::{ClientCacheKey, ClientCacheEntry};

//Initial realm until we get the client realm sent through
//static REALM: &str = "sa-teams";

///Represents the oicd connect token
pub struct AuthToken(pub String);

pub struct Permissions {
    pub mappings: Vec<Provider>
}


/// Given an auth token, a method name, the security cache and the permissions mapping configuration
/// determine if the method call can be made.
/// # Arguments
/// sec_cacher the security cache
/// permissions_mappings the security configuration
/// method_name The name of the rest service being called
/// auth_token The auth token provided by the client
/// # Return
/// The go/no go result
pub fn permissions(sec_clients: &Arc<Mutex<LruCache<ClientCacheKey, ClientCacheEntry>>>, permissions_mapping: &Vec<Provider>,
                   method_name: &str, requested_permissions: Vec<Permission>, auth_token: &AuthToken, realm: &str, provider: &str) -> Result<bool, String>{
    let auth_token = &auth_token.0;

    trace!("token {:?}", auth_token);

    let mappings_result = find_mapped_permissions(provider, method_name,
                                                   &permissions_mapping);
    if mappings_result.is_ok() {

        let mappings = &mappings_result.unwrap();
        let checked_permissions = check_permissions(&requested_permissions, &mappings.common_access);

        if checked_permissions {
            let resource_name = &mappings.resource_name;
            let client_key: ClientCacheKey = ClientCacheKey::new(&auth_token, method_name);
            let mut lock = sec_clients.lock();
            let client = lock.as_mut().unwrap().peek(&client_key);

            match client {
                Some(c) => {info!("The client was in cache!!");
                            
                            if c.expired() {
                                lock.as_mut().unwrap().pop(&client_key);
                                drop(lock);
                                error!("Security cache entry expired");
                                Err(String::from("Cache entry expired"))
                            } else {
                                info!("Security cache entry not expired");
                                if c.client.has_resource_permission(resource_name) {
                                    Ok(true)
                                } else {
                                    Err(format!("No permission for {}", resource_name))
                                }
                            }
                },
                None => {info!("The client was NOT FOUND in cache!!");
                         let client: Result<Box<dyn Client + Send + Sync>, ProviderError> =
                            get_client(Providers::Keycloak, Token{val: auth_token.clone()}, realm);
                         if client.is_ok() {
                                info!("Client created");
                                let entry = ClientCacheEntry::new(auth_token, realm);
                                info!("made entry for client with name: {:?}", entry.client.name());
                                lock.unwrap().put(client_key, entry);
                                info!("Client added to cache");
                                if client.unwrap().has_resource_permission(resource_name) {
                                    Ok(true)
                                } else {
                                    Err(format!("No permission for {}", resource_name))
                                }
                          } else {error!("Client was not in cache and could not make client.");
                                  Err(String::from("Could not make client."))
                          }
                },
            }
        } else {
            error!("Requested permissions were not configured {:?}", &requested_permissions);
            warn!("Requested permissions were not configured {:?}", &requested_permissions);
            Err(String::from("Finding mapping permissions failed"))
        }

    } else {
        error!("No permissions map found");
        Err(String::from("Finding mapping permissions failed"))
    }

}
