//Copyright SoftwareByNumbers Ltd 2021

//! A trait providing an abstraction of a security service.
//!
//!

use super::adaptor::{keycloak, dummy};
use core::marker::{Send, Sync};

#[derive(Debug,Clone)]
pub enum ProviderError {
    InvalidToken,
}

/// The supported providers.
#[derive(Debug)]
pub enum Providers {
    Keycloak,
    Dummy
}

#[derive(Debug,Clone)]
pub struct Token {
    pub val: String
}

///Is the client allowed to actually view a data item.
#[derive(Debug,Clone)]
pub enum ViewState <'a>{
    //The client can know of the existence
    Yes,
    //The client cannot know of the existence
    No,
    //The client sees this message instead of the data.
    Message(&'a str)
}

///Access permissions
#[derive(Debug)]
pub enum Access<'a>{
    Create(bool),
    Read(bool),
    Update(bool),
    Delete(bool),
    View(ViewState<'a>)
}

///Map a resource to its access permissions and the resource name
#[derive(Debug)]
pub struct Resource<'a> {
    common_name: &'a str,           //should be a provider neutral name
    resource_name: &'a str,         //should map to providers resource name
    access: Vec::<Access::<'a>>,    //map the crud capabilities to the resource
    provider_type: Providers,       //identify the provider(keycloak etc)
}

impl<'a> Resource<'a> {
    //! Make a new resource
    pub fn new_resource(name: &'a str, r_name: &'a str, access: Access::<'a>, provider: Providers) -> Resource::<'a> {
        Resource{common_name: name,
                 resource_name: r_name,
                 access: vec!(access),
                 provider_type: provider}
    }
}


impl<'a> PartialEq for Access<'a> {
    fn eq(&self, other: &Self) -> bool {
        use Access::*;
        use ViewState::*;
        match (self, other) {
            (Create(a), Create(b)) => a == b,
            (Read(a), Read(b)) => a == b,
            (Update(a), Update(b)) => a == b,
            (Delete(a), Delete(b)) => a == b,
            (View(Yes), View(Yes)) => true,
            (View(No), View(No)) => true,
            _ => false,
        }
    }
}

///The security provider abstraction
pub trait Client<'a> {
    fn op_set(&self) -> &[Access<'a>];
    fn name(&self) -> &str;
    fn provider(&self, client_token: Token, realm: &str) -> Result<Box<dyn Client<'a> + Send + Sync>, ProviderError>;
    fn has_resource_permission(&self, requested_resource: &str) -> bool;
}

/// Get a concrete implementation of a security client.
/// #Arguments
/// provider which provider to create
/// #Return
/// A result with the client impl.
pub fn get_client<'a>(provider: Providers, client_token: Token, realm: &str) -> Result<Box<dyn Client<'a> + Send + Sync>, ProviderError> {

  match provider {
      Providers::Keycloak => {let client = keycloak::ClientRepresentation::service_client(client_token, realm)?;
                              Ok(Box::new(client) as Box<dyn Client<'a> + Send + Sync>)
      }
      Providers::Dummy    => {let client = dummy::ClientRepresentation::service_client(client_token, realm)?;
                              Ok(Box::new(client) as Box<dyn Client<'a> + Send + Sync>)
     }
  }
}


#[cfg(test)]
mod provider_tests {

    use super::*;

    #[test]
    fn test_valid_provider() {
        let test_token_id = String::from("test_token_1");
        let test_token = Token{val: test_token_id};
        let client1 = get_client(Providers::Dummy, test_token.clone(), &"my_realm");
        let client2 = get_client(Providers::Keycloak, test_token, &"my_realm");
        assert!(client1.is_ok());
        assert!(client2.is_ok());
        assert_eq!(Client::name(client1.unwrap().as_ref()), String::from("Dummy"));
        assert_eq!(Client::name(client2.unwrap().as_ref()), "Keycloak");
    }

    #[test]
    fn test_dummy_mappings() {
        let test_token_id = String::from("test_token_1");
        let test_token = Token{val: test_token_id};
        let client = get_client(Providers::Dummy, test_token, &"my_realm");
        assert!(client.is_ok());
        let can_json_to_md = Resource::new_resource(&"can_json_to_md", &"json_to_md", Access::Read(true), Providers::Dummy);
        println!("can resource: {:?}", can_json_to_md);
        let dummy = client.unwrap();
        assert!(dummy.name() == "Dummy");
        dummy.op_set().contains(&Access::Read(true));
    }

    #[test]
    fn test_keycloak_mappings() {
        let test_token_id = String::from("test_token_1");
        let test_token = Token{val: test_token_id};
        let client = get_client(Providers::Keycloak, test_token, &"my_realm");
        assert!(client.is_ok());
        let can_json_to_md = Resource::new_resource(&"can_json_to_md", &"json_to_md", Access::Read(true), Providers::Keycloak);
        println!("can resource: {:?}", can_json_to_md);
        let keycloak = client.unwrap();
        assert!(keycloak.name() == "Keycloak");
        keycloak.op_set().contains(&Access::Read(true));
    }
}
