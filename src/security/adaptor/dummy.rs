//! A dummy security provider for testing.


use super::super::security_adaptor::{Client, Access, ViewState, Token, ProviderError};

//Dummy access set for now
static SECURITY_BREACH: &str = "Security Breach!!";
static DEFAULT_ACCESS_SET: [Access;2] = [Access::Create(true), Access::View(ViewState::Message(SECURITY_BREACH))];


pub struct ClientRepresentation {
    token: Token,
    name: String,
}

impl ClientRepresentation {
    pub fn service_client(client_token: Token, _real: &str) -> Result<ClientRepresentation, ProviderError> {
        Ok(ClientRepresentation {token: client_token, name: String::from("Dummy")})
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn token(&self) -> &Token {
        &self.token
    }
}

//Get a representation of the client using the client token.
impl<'a> Client<'a> for ClientRepresentation {
    fn op_set(&self) -> &[Access<'a>] {
        &DEFAULT_ACCESS_SET
    }
    fn name(&self) -> &str {
        &self.name
    }
    fn provider(&self, client_token: Token, realm: &str) -> Result<Box<dyn Client<'a> + Send + Sync>, ProviderError> {
        let client = ClientRepresentation::service_client(client_token, realm)?;
        Ok(Box::new(client) as Box<dyn Client<'a> + Send + Sync>)
    }
    fn has_resource_permission(&self, _requested_resource: &str) -> bool {
        false
    }
}
