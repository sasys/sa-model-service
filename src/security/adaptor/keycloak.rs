//Copyright SoftwareByNumbers Ltd 2021

//! Adaptor for the keycloak security system
//!
//! A concrete implementation of the provider client trait.

use log::{info, debug, error};
use http;
use super::super::security_adaptor::{Client, Access, ViewState, Token, ProviderError};

use serde_json;
use serde::{Deserialize, Serialize};

/// Security breach message
static SECURITY_BREACH: &str = "Security Breach!!";

/// default access set
static DEFAULT_ACCESS_SET: [Access;2] = [Access::Create(true), Access::View(ViewState::Message(SECURITY_BREACH))];

///The oicd token endpoint for keycloak.  NOTE REALM is to be substituted
static KEYCLOAK_TOKEN_ENDPOINT: &str = "http://192.168.0.12:8080/auth/realms/REALM/protocol/openid-connect/token";
static REQUEST_STRING: &str = "grant_type=urn:ietf:params:oauth:grant-type:uma-ticket&audience=sa-model-service&response_include_resource_name=true&response_mode=permissions";

/// Replace the REALM marker with the given realm or just return the string if the REALM
/// marker is not present
fn replace_realm(url: &str, realm: &str) -> String {
    if url.contains("REALM")
        {str::replace(url, "REALM", realm)
    } else {
        String::from(url)
    }
}

///Keycloak permissions
#[derive(Serialize, Deserialize, Debug)]
struct Permissions {
    rsid: String,
    rsname: String
}

fn parse_permissions(permissions: &str) -> Result<Vec<Permissions>, String> {

    let perms_res: Result<Vec::<Permissions>, serde_json::Error> = serde_json::from_str(permissions);

    if perms_res.is_ok() {
        let perms = perms_res.unwrap();
        info!("Unmarshaled permissions is an array: {:?}", perms);
        Ok(perms)
    } else {
        Err(String::from("Failed to unmarshal the permissions array."))
    }
}

fn connect(token: &str, realm: &str) -> Result<Vec<Permissions>, String> {
    debug!("KEYCLOAK CLIENT CONNECTING");
    let bearer_token = "Bearer ".to_owned() + token;
    let mut headers: http::HeaderMap = http::HeaderMap::new();
    headers.insert(http::header::ACCEPT, http::HeaderValue::from_str("application/json").unwrap());
    headers.insert(http::header::HOST, http::HeaderValue::from_str("sa.softwarebynumbers.com:8080").unwrap());
    headers.insert(http::header::AUTHORIZATION, http::HeaderValue::from_str(&bearer_token).unwrap());
    headers.insert(http::header::CONTENT_TYPE, http::HeaderValue::from_str("application/x-www-form-urlencoded").unwrap());
    let patched_url = replace_realm(KEYCLOAK_TOKEN_ENDPOINT, realm);
    let res = reqwest::blocking::Client::new().post(&patched_url).headers(headers).body(REQUEST_STRING).send();
    match res  {
        Ok(r) => {match r.text() {
                    Ok(t) => {let permissions = parse_permissions(&t);
                              info!("Parsed permissions: {:?}", permissions);
                              permissions}
                    Err(e) => {error!("KEYCLOAK   Error res {}", e);
                               Err(format!("KEYCLOAK   Error res {}", e))},
                }},
        Err(e) => {error!("Failed err: {}", e);
                   Err(format!("Failed err: {}", e))}

    }
}

///A representation of a keycloak client
pub struct ClientRepresentation {
    token: Token,
    name:  String,
    keycloak_url: String,
    permissions: Vec<Permissions>
}

impl<'a> ClientRepresentation {
    pub fn service_client(client_token: Token, realm: &str) -> Result<ClientRepresentation, ProviderError> {
        let permissions: Result<Vec<Permissions>, String> = connect(&client_token.val, realm);
        if permissions.is_ok() {
            Ok(ClientRepresentation {token: client_token, name: String::from("Keycloak"), keycloak_url: replace_realm(KEYCLOAK_TOKEN_ENDPOINT, realm), permissions: permissions.unwrap()})
        } else {
            Ok(ClientRepresentation {token: client_token, name: String::from("Keycloak"), keycloak_url: replace_realm(KEYCLOAK_TOKEN_ENDPOINT, realm), permissions: Vec::new()})
        }

    }

    pub fn token(&self) -> &Token {
        &self.token
    }
    pub fn keycloak_url(&self) -> &str {
        &self.keycloak_url
    }
}

///A representation of the generic client for keycloak using the client token.
impl<'a> Client<'a> for ClientRepresentation {
    fn op_set(&self) -> &[Access<'a>] {
        &DEFAULT_ACCESS_SET
    }
    fn name(&self) -> &str {
        &self.name
    }

    fn provider(&self, client_token: Token, realm: &str) -> Result<Box<dyn Client<'a> + Send + Sync>, ProviderError> {
        let client = ClientRepresentation::service_client(client_token, realm)?;
        Ok(Box::new(client) as Box<dyn Client<'a> + Send + Sync>)
    }

    fn has_resource_permission(&self, requested_resource: &str) -> bool {
        info!("Permissions {:?}", self.permissions);
        info!("Requested resource: {}", requested_resource);
        self.permissions.iter().filter(|p| p.rsname.eq_ignore_ascii_case(requested_resource)).count() > 0
    }

}
