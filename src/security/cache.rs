//Copyright SoftwareByNumbers Ltd 2021

//! A cache to hold on to security connections intended to prevent repeated
//! connections to the security provider.
//!
//! The cache is behind a Mutex.
//!
//! ## Example
//!
//!```rust
//!
//! let cache:  Arc<Mutex<LruCache<ClientCacheKey, ClientCacheEntry<'a>>>> = get_cache
//! let entry = ClientCacheEntry::new("123", "my-realm");
//! let key = ClientCacheKey::new("123", "some-method");
//!
//! assert_eq!(cache.lock().unwrap().len(), 0);
//! cache.lock().put(key, entry);
//! assert_eq!(cache.lock().unwrap().len(), 1);
//! ```


use core::marker::{Send, Sync};
use std::sync::{Mutex, Arc};

use lru::LruCache;

use super::security_adaptor::{Token, Providers, Client, get_client, ProviderError};

static CACHE_ENTRY_EXPIRES_DELTA: i64 = 60;  //seconds

static DEFAULT_CACHE_SIZE: usize = 10;

pub struct ClientCacheEntry<'a> {
    _token: String,
    expires: i64,
    pub client: Box<dyn Client<'a> + Send + Sync>
}

impl<'a> ClientCacheEntry<'a> {
    /// Creates a new key for the client hashmap
    pub fn new(token: &str, realm: &str) -> ClientCacheEntry<'a> {
        let client_: Result<Box<dyn Client + Send + Sync>, ProviderError> = get_client(Providers::Keycloak, Token{val: String::from(token)}, realm);
        let time = chrono::Local::now();
        ClientCacheEntry { _token: token.to_string(), expires: time.timestamp() + CACHE_ENTRY_EXPIRES_DELTA, client: client_.unwrap() }
    }

    pub fn expired(&self) -> bool {
        let t = chrono::Local::now().timestamp();
        t > self.expires
    }

}

// This is the custom key for our hashmap
#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct ClientCacheKey {
    token: String,
    method: String
}

impl ClientCacheKey {
    /// Creates a new key for the client hashmap
    pub fn new(token: &str, method: &str) -> ClientCacheKey {
        ClientCacheKey { token: token.to_string(), method: method.to_string() }
    }
}

// Get a client security cache
//
// #Return
// A Mutex protected cache
pub fn get_cache<'a>() ->  Arc<Mutex<LruCache<ClientCacheKey, ClientCacheEntry<'a>>>> {

    // The cache size is arbitrary and needs to go into the config or be on a per domain basis.
    Arc::new(Mutex::new(LruCache::new(DEFAULT_CACHE_SIZE)))
}


#[cfg(test)]
mod cache_tests {

    use super::*;

    #[test]
    fn test_empty_cache<'a>() {
        let cache:  Arc<Mutex<LruCache<ClientCacheKey, ClientCacheEntry<'a>>>> = get_cache();
        assert_eq!(cache.lock().unwrap().cap(), 10);
        assert!(cache.lock().unwrap().is_empty());
        assert_eq!(cache.lock().unwrap().len(), 0);
    }
}
