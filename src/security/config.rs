//Copyright SoftwareByNumbers Ltd 2021

//! Handles a file of security configuration ie the CRUD capabilities for a service

extern crate config;

use std::fs;
use config::{Config, File, ConfigError};
use serde::{Serialize, Deserialize};
use log::{info, error};
use std::collections::HashSet;

pub static JSON_TO_MARKDOWN_PERMISSIONS: &str = "resources/provider_configuration.json";

pub static GENERAL_CONFIG: &str = "resources/config.json";

pub static SECURITY_PROVIDER_KEY: &str = "security_provider";

pub static DEFAULT_REALM_KEY: &str = "default_realm";

/// Read the general configuration from the given relative file path
/// # Arguments
/// file_name is the path to the general config error
/// # Return
///
fn read_config<'a>(file_name: &str) -> Result<Config, ConfigError> {
    let mut settings = Config::new();
    let f = File::with_name(file_name);
    match settings.merge(f) {
        Ok(s) => Ok(s.clone()),
        Err(e) => Err(e)
    }
}

/// Read the general configuration file and return the GeneralConfiguration struct.
/// # Arguments
/// file_name the path to the general configuration file.
/// # Return
/// A result that contains the general configuration or an error.
pub fn read_general_config(file_name: &str) -> Result<GeneralConfig, String> {

    match read_config(file_name) {
        Ok(s)  => {
            let (s,r) = (Config::get_str(&s, SECURITY_PROVIDER_KEY), Config::get_str(&s, DEFAULT_REALM_KEY));
            match (s,r)  {
                (Ok(p), Ok(r))  => Ok(GeneralConfig{security_provider: p, default_realm: r}),
                (Err(_),Err(_)) => Err(String::from("Could not read security provider and realm key")),
                (_,_)           => Err(String::from("Could not read security provider or realm key, or both"))
            }
        }
        Err(_) => Err(String::from(format!("Could not read general config file at {}", file_name)))
    }
}

pub struct GeneralConfig {
    pub security_provider: String,
    pub default_realm: String
}



/// Read the permissions mapping from the given permissions mapping file
///
/// # Arguments
/// permissions_filename raltive path to the permissions mapping file
/// # Return
/// A result on the provider struct or an error
pub fn read_permissions_mappings(permissions_filename: &str) -> Result<Vec<Provider>, String> {
    match fs::read_to_string(permissions_filename) {
        Ok(j) =>{info!("Reading jason permissions mapping file : {}", permissions_filename);
                 let deserializer = &mut serde_json::Deserializer::from_str(&j);
                 let result: Result<Vec<Provider>, _> = serde_path_to_error::deserialize(deserializer);

                 match result {
                        Ok(s) => {Ok(s)},
                        Err(e) => {let path = e.path().to_string();
                                   info!("Path to unmarshal error : {}", path);
                                   Err(format!("Failed, path {} ", path))
                        }
                 }},
        Err(e) => {
            println!("Failed to read file : {} with error {}", permissions_filename, e);
            Err(format!("Failed to read permissions config {}", &permissions_filename))
        }
    }
}

// These are the logical actions that a service might want to perform, they may be grouped,
// for eample, a create followed by an update followed by another update. ie a service could be
// compound of other services.
#[derive(Serialize, Deserialize, Clone, Hash, Eq, PartialEq, Debug)]
pub enum Permission {
    Create,
    Read,
    Update,
    Delete
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Method {
    name: String,
    description: String,
    pub resource_name: String,
    pub common_access: Vec<Permission>
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Service {
    notes: String,
    methods: Vec<Method>
}

/// Note that the realm should match the security provider realm name.  This allows the service
/// to work ona specific realm.
#[derive(Serialize, Deserialize, Clone)]
pub struct Provider {
    provider: String,
    realm: String,
    token_endpoint: String,
    services: Service
}


pub fn find_method<'a>(method_name: &str, service: &'a Service) -> Option<&'a Method> {
    service.methods.iter().filter(|m| m.name.eq_ignore_ascii_case(method_name)).next()
}

pub fn realm<'a>(provider: &'a Provider) -> &str {
    &provider.realm
}

pub fn provider<'a>(name: &str, providers: &'a Vec<Provider>) -> Option<&'a Provider> {
    providers.iter().filter(|p| p.provider.eq_ignore_ascii_case(name)).next()
}

//Check that all the requested access permissions ar ein the common access permissions
pub fn check_permissions(requested_access: &Vec<Permission>, common_access: &Vec<Permission>) -> bool {
    if requested_access.len() > 0 && common_access.len() > 0 {
        let granted: HashSet<&Permission> = common_access.iter().collect();
        let requested: HashSet<&Permission> = requested_access.iter().collect();
        requested.is_subset(&granted)
    } else {
        false
    }

}

pub fn find_mapped_permissions<'a>(provider_name: &str, method_name: &str, providers: &'a Vec<Provider>) -> Result<&'a Method, String> {
    //The provider
    match provider(provider_name, providers) {
        Some(p) => {
            info!("Found provider.");
            match find_method(method_name, &p.services) {
                Some(m) => {Ok(m)},
                None => {Err(format!("No method {} found for provider {}",method_name, provider_name))}
            }
        }
        None    => {error!("Provider not found");
                    Err(format!("No provider found for {}", provider_name))}
    }
}

#[cfg(test)]
mod general_config_tests {

    use super::*;

    static TEST_CONFIG: &str = "resources/test/config.json";

    #[test]
    fn test_read_config_file() {
        let config = read_config(TEST_CONFIG);
        assert!(config.is_ok());
        let found = Config::get_str(&config.unwrap(), SECURITY_PROVIDER_KEY);
        print!("Found {:?}", found);
        assert!(found.is_ok());
        assert_eq!(found.unwrap(), "keycloak");
    }

    #[test]
    fn test_read_general_config_file() {
        let config = read_general_config(TEST_CONFIG);
        assert!(config.is_ok());
        assert_eq!(config.unwrap().security_provider, "keycloak");
    }
}

#[cfg(test)]
mod permissions_config_tests {

    use super::*;

    static JSON_TO_MARKDOWN_PERMISSIONS: &str = "resources/test/provider_configuration.json";

    static TEST_JSON_CONFIG: &str = r#"[{"provider": "keycloak",
      "realm": "sa-teams",
      "token_endpoint": "None",
      "services": {"notes": "REST methods for the sa-md service",
                   "methods": [{"name":          "modeltomd",
                                "description":   "Describes the allowed access by the ui client to the json to markdown convertor",
                                "resource_name": "json_to_md",
                                "common_access": ["Create", "Read"]},
                               {"name":           "mdtomodel",
                                "description": "Describes the allowed access by the ui client to the json to markdown convertor",
                                "resource_name": "md_to_json",
                                "common_access": ["Create", "Read"]}
                              ]
                 }
     },
     {"provider": "dummy",
      "realm": "sa-teams",
      "token_endpoint": "None",
      "services": {"notes": "REST methods for a mock service",
                   "methods": [{"name": "convert_json_to_markdown",
                                "description": "Describes the allowed access by the ui client to the json to markdown convertor",
                                "resource_name": "json_to_md",
                                "common_access": ["Create", "Read"]},
                               {"name": "convert_markdown_to_json",
                                "description": "Describes the allowed access by the ui client to the json to markdown convertor",
                                "resource_name": "md_to_json",
                                "common_access": ["Create", "Read"]}
                              ]
                  }
     }]"#;

    #[test]
    fn test_unmarshal_json() {
        let deserializer = &mut serde_json::Deserializer::from_str(TEST_JSON_CONFIG);
        let result: Result<Vec<Provider>, _> = serde_path_to_error::deserialize(deserializer);
        match result {
            Ok(_) => (),
            Err(err) => {
                let path = err.path().to_string();
                assert_eq!(path, "dependencies.serde.version"); //just show the path
            }
        }
    }

    #[test]
    fn test_read_permissions_config()  {

        match read_permissions_mappings(JSON_TO_MARKDOWN_PERMISSIONS) {
            Ok(p) => {assert!(p.len() == 2);
                      assert!(p[0].provider == "keycloak");
                      let servs = &p[0].services;
                      assert!(servs.notes == "REST methods for the sa-md service");
                      let mthds = &servs.methods;
                      let m1 = &mthds[0];
                      assert!(mthds.len() == 2);
                      assert!(m1.resource_name == "json_to_md");
                      assert!(m1.description == "Describes the allowed access by the ui client to the json to markdown convertor");
                      assert!(m1.name == "modeltomd");
                      assert!(m1.common_access == vec!(Permission::Create, Permission::Read))},
            Err(e) => {println!("Error reading config {}", e);
                       assert!(false)}
        }
    }

    #[test]
    fn test_find_method()  {
        let method: Method = Method {name: String::from("modeltomd"),
                                     description: String::from("Describes the..."),
                                     resource_name: String::from("json_to_md"),
                                     common_access: vec!(Permission::Create, Permission::Read)
        };
        let service = Service {notes: String::from("Some notes"),
                               methods: vec!(method)
        };
        let res = find_method("modeltomd", &service);
        assert!(res.is_some());
        let res = find_method("nope", &service);
        assert_eq!(res.is_some(), false);
    }

    #[test]
    fn test_providers()  {
        let method: Method = Method {name: String::from("modeltomd"),
                                     description: String::from("Describes the..."),
                                     resource_name: String::from("json_to_md"),
                                     common_access: vec!(Permission::Create, Permission::Read)
        };
        let service = Service {notes: String::from("Some notes"),
                               methods: vec!(method)
        };
        let prvdr   = Provider {provider: String::from("keycloak"),
                                realm: String::from("sa-teams"),
                                token_endpoint: String::from("none"),
                                services: service
        };
        let providers = vec!(prvdr);
        let res = provider("keycloak", &providers);
        assert!(res.is_some());
        let res = provider("nope", &providers);
        assert_eq!(res.is_some(), false);
    }

    #[test]
    fn test_find_mapped_permissions()  {
        let method: Method = Method {name: String::from("modeltomd"),
                                     description: String::from("Describes the..."),
                                     resource_name: String::from("json_to_md"),
                                     common_access: vec!(Permission::Create, Permission::Read)
        };
        let service = Service {notes: String::from("Some notes"),
                               methods: vec!(method)
        };
        let prvdr   = Provider {provider: String::from("keycloak"),
                                realm: String::from("sa-teams"),
                                token_endpoint: String::from("none"),
                                services: service
        };
        let providers = vec!(prvdr);

        let permissions = find_mapped_permissions("keycloak", "modeltomd", &providers);

        assert!(permissions.is_ok());
        let ok_perms = permissions.unwrap();
        println!("PERMS NAME: {:?}", ok_perms.name);
        println!("PERMS DESC: {:?}", ok_perms.description);
        println!("PERMS RESOURCE_NAME: {:?}", ok_perms.resource_name);
        println!("PERMS ACCESS: {:?}", ok_perms.common_access);
    }

    #[test]
    fn test_check_permissions() {
        //pub fn check_permissions(requested_access: Vec<Permission>, common_access: Vec<Permission>) -> bool {
        let request_permissions_1 = vec!(Permission::Read, Permission::Update);
        let request_permissions_2: Vec<Permission> = vec!();
        let granted_access_1 = vec!();
        let granted_access_2 = vec!(Permission::Read, Permission::Delete);
        let granted_access_3 = vec!(Permission::Create, Permission::Read, Permission::Update, Permission::Delete);
        assert_eq!(false, check_permissions(&request_permissions_1, &granted_access_1));
        assert!(check_permissions(&request_permissions_1, &granted_access_3));
        assert_eq!(false, check_permissions(&request_permissions_2, &granted_access_3));
        assert_eq!(false, check_permissions(&request_permissions_1, &granted_access_1));
        assert_eq!(false, check_permissions(&request_permissions_1, &granted_access_2));
    }
}
