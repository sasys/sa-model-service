//Copyright SoftwareByNumbers Ltd 2021

//! # Structs related to model meta data
//!
//! Data structures related to manipulation of the model meta data.
//!
//! Specifies how to serialise and deserialize these types to make them
//! inter-work between rust, clojure and couch db.
//!
//!

use serde::{Serialize, Deserialize};

///Models the meta data associated with a model
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ModelMetaData {
    #[serde(rename(serialize = "sadf/summary"))]
    #[serde(alias = "sadf/summary")]
    #[serde(alias = "summary")]
    #[serde(alias = "sa-draw.config.defaults/summary")]
    pub summary: String,
    #[serde(rename(serialize = "sadf/app-version"))]
    #[serde(alias = "sadf/app-version")]
    #[serde(alias = "app-version")]
    #[serde(alias = "sa-draw.config.defaults/app-version")]
    pub app_version: Version,
    #[serde(default)]
    #[serde(rename(serialize = "sadf/model-version"))]
    #[serde(alias = "sadf/model-version")]
    #[serde(alias = "model-version")]
    #[serde(alias = "sa-draw.config.defaults/model-version")]
    pub model_version: Option<Version>,
    #[serde(rename(serialize = "sadf/creation-date-time"))]
    #[serde(alias = "sadf/creation-date-time")]
    #[serde(alias = "creation-date-time")]
    #[serde(alias = "sa-draw.config.defaults/creation-date-time")]
    pub creation_date_time: String,
    #[serde(rename(serialize = "sadf/uri"))]
    #[serde(alias = "sadf/uri")]
    #[serde(alias = "sa-draw.config.defaults/uri")]
    pub uri: Option<String>,
    #[serde(rename(serialize = "sadf/uuid"))]
    #[serde(alias = "sadf/uuid")]
    #[serde(alias = "uuid")]
    #[serde(alias = "sa-draw.config.defaults/uuid")]
    pub uuid: String,
    #[serde(rename(serialize = "sadf/namespace"))]
    #[serde(alias = "sadf/namespace")]
    #[serde(alias = "namespace")]
    #[serde(alias = "sa-draw.config.defaults/namespace")]
    pub namespace: String,
    #[serde(default)]
    #[serde(rename(serialize = "sadf/board-link"))]
    #[serde(alias = "sadf/board-link")]
    #[serde(alias = "board-link")]
    #[serde(alias = "sa-draw.config.defaults/board-link")]
    pub board_link: Option<String>,
    #[serde(rename(serialize = "sadf/detail"))]
    #[serde(alias = "sadf/detail")]
    #[serde(alias = "detail")]
    #[serde(alias = "sa-draw.config.defaults/detail")]
    pub detail: Detail,
    #[serde(rename(serialize = "sadf/name"))]
    #[serde(alias = "sadf/name")]
    #[serde(alias = "name")]
    #[serde(alias = "sa-draw.config.defaults/name")]
    pub name: String,
}


///Versioning information
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Version {
    #[serde(rename(serialize = "sa-draw.config.defaults/major"))]
    #[serde(alias = "sa-draw.config.defaults/major")]
    pub major: u16,
    #[serde(rename(serialize = "sa-draw.config.defaults/minor"))]
    #[serde(alias = "sa-draw.config.defaults/minor")]
    pub minor: u16,
    #[serde(rename(serialize = "sa-draw.config.defaults/patch"))]
    #[serde(alias = "sa-draw.config.defaults/patch")]
    pub patch: u16,
    #[serde(default)]
    #[serde(rename(serialize = "sa-draw.config.defaults/pre-release"))]
    #[serde(alias = "sa-draw.config.defaults/pre-release")]
    pub pre_release: Option<String>,
    #[serde(default)]
    #[serde(rename(serialize = "sa-draw.config.defaults/build-meta"))]
    #[serde(alias = "sa-draw.config.defaults/build-meta")]
    pub build_meta: Option<String>,
}


impl Version {
    pub fn new(version: &Version) -> Version {
        Version{major: version.major.clone(),
                minor: version.minor.clone(),
                patch: version.patch.clone(),
                pre_release: version.pre_release.clone(),
                build_meta: version.build_meta.clone()}
    }
}

impl PartialEq for Version {
    fn eq(&self, other: &Self) -> bool {
        self.major == other.major &&
        self.minor == other.minor &&
        self.patch == other.patch &&
        self.pre_release == other.pre_release &&
        self.build_meta == other.build_meta
    }
}
impl Eq for Version {}



///Models the nominated ownership of the model
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Detail {
    #[serde(rename(serialize = "sadf/ownership"))]
    #[serde(alias = "sadf/ownership")]
    pub ownership: Option<String>,
    #[serde(rename(serialize = "sadf/contact"))]
    #[serde(alias = "sadf/contact")]
    pub contact: Option<String>,
    #[serde(rename(serialize = "sadf/contact-email"))]
    #[serde(alias = "sadf/contact-email")]
    pub contact_email: Option<String>,

    //This one has been problematic - my server code was adding both
    //sadf/uri and uri which causes a failure here.  Have put a fix in the
    // clojure server code for this.
    #[serde(rename(serialize = "sadf/uri"))]
    #[serde(alias = "sadf/uri")]
    pub uri: Option<String>,
}

impl PartialEq for Detail {
    fn eq(&self, other: &Self) -> bool {
        self.ownership == other.ownership &&
        self.contact == other.contact &&
        self.contact_email == other.contact_email &&
        self.uri == other.uri
    }
}
impl Eq for Detail {}


///A simple contact struct
#[derive(Serialize, Deserialize, Debug)]
pub struct Contact {
    #[serde(rename(serialize = "sadf:user-name"))]
    #[serde(alias = "sadf:user-name")]
    #[serde(alias = "user-name")]
    pub user_name: String,
    pub phone : String,
}

impl PartialEq for Contact {
    fn eq(&self, other: &Self) -> bool {
        self.user_name == other.user_name && self.phone == other.phone
    }
}
impl Eq for Contact {}

///Model state description
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ModelState {
    pub uri:  String,
    pub uuid: String,
    pub has_model_version: bool,
    pub has_inner_uri: bool,
    pub has_outer_uri: bool
}


#[derive(Serialize, Deserialize, Debug)]
pub struct ModelStates {
    pub states: Vec<ModelState>
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::{Value};
    use rocket_contrib::json::Json;

    //Test serde json serialisation because its very confusing!!
    //Sanity checking my understanding.
    #[test]
    fn test_example_contact_serialization_deserialization() {

        //A string bases contact with a hyphen in a key
        let good_contact_data = r#"{"user-name": "fred", "phone": "2222"}"#;

        //A types contact with a rust style key
        let good_contact_data_1 : Contact = Contact{user_name: String::from("simon"),
                                                    phone: String::from("3333")};

        //A copy of the above
        let good_contact_data_2 : Contact = Contact{user_name: String::from("simon"),
                                                    phone: String::from("3333")};

        //from the string form as a value where the keyword is not rusty!!!
        let good_contact: Value = serde_json::from_str(good_contact_data).unwrap();

        //from the struct form as json
        let good_contact_1: Json<Contact> = Json(good_contact_data_1);

        //Take the contact with a rusty style key and serialize it with a clojure style hyphenated key
        let good_contact_2_r = serde_json::to_string(&good_contact_data_2);
        //let good_contact_2 = serde_json::to_string(&good_contact_data_2).unwrap();
        let good_contact_2 = good_contact_2_r.unwrap();
        let good_contact_2_value: Value = serde_json::from_str(good_contact_2.as_str()).unwrap();

        //roundtrip the above back to a rust struct with rusry style key

        let good_contact_2_deser_r: serde_json::Result<Contact> = serde_json::from_str(&good_contact_2);

        println!("ser: {}", good_contact_2);
        println!("deser: {:?}", good_contact_2_deser_r);

        //from the struct form as a contact
        //NOTE you cant do Json(good_contact_data_1) here.
        //let good_contact_2: Contact = serde_json::from_str(good_contact_data).unwrap();

        assert_eq!(good_contact.is_object(), true);
        assert_eq!(good_contact["user-name"], "fred");
        assert_eq!(good_contact_1.user_name, "simon");

        //test the round trip
        assert!(good_contact_2.contains("user-name"));              //the non-rusty key
        assert_eq!(good_contact_2_value["sadf:user-name"],"simon");     //the name spaced key
        assert_eq!(good_contact_2_deser_r.is_ok(), true);               //deserialized as a rust struct ok
        let good_contact_2_deser = good_contact_2_deser_r.unwrap(); //deserialized as a rust struct
        assert_eq!(good_contact_2_deser, good_contact_data_2);          //same as the original - ie round tripped ok
        assert_eq!(good_contact_2_deser.user_name, "simon");            //Has a correct user_name
    }

    #[test]
    fn test_detail_serialization_deserialization() {
        let detail_data_1 = r#"{"ownership": "business-dept", "uri": "a/b/c", "contact-email": "a@b.com", "contact": "Micky"}"#;
        let detail_data_2 = r#"{"sadf:ownership": "business-dept",
                                "sadf:uri": "a/b/c",
                                "sadf:contact-email": "a@b.com",
                                "sadf:contact": "Micky"}"#;

        let detail_data_3: Detail =  Detail{ownership: Some(String::from("business-dept")),
                                            uri: Some(String::from("a/b/c")),
                                            contact_email: Some(String::from("a@b.com")),
                                            contact: Some(String::from("Micky"))};

        //from the string form as a value where the keyword is not rusty!!!
        let detail_1_value: Value = serde_json::from_str(detail_data_1).unwrap();

        //from the string form as a value where the keyword is not rusty and is name spaced!!!
        let detail_2_value: Value = serde_json::from_str(detail_data_2).unwrap();

        //from the struct form as json
        let detail_3_json: Json<Detail> = Json(detail_data_3);


        assert_eq!(detail_1_value["ownership"], "business-dept");
        assert_eq!(detail_1_value["uri"], "a/b/c");
        assert_eq!(detail_1_value["contact-email"], "a@b.com");
        assert_eq!(detail_1_value["contact"], "Micky");

        assert_eq!(detail_2_value["sadf:ownership"], "business-dept");
        assert_eq!(detail_2_value["sadf:uri"], "a/b/c");
        assert_eq!(detail_2_value["sadf:contact-email"], "a@b.com");
        assert_eq!(detail_2_value["sadf:contact"], "Micky");

        assert_eq!(detail_3_json.ownership, Some(String::from("business-dept")));
        assert_eq!(detail_3_json.uri, Some(String::from("a/b/c")));
        assert_eq!(detail_3_json.contact_email, Some(String::from("a@b.com")));
        assert_eq!(detail_3_json.contact, Some(String::from("Micky")));
    }



    #[test]
    fn test_version_serialization_deserialization() {
        let version_data_1 = r#"{"major": 22, "minor": 6, "patch": 12, "pre-release": "Trial", "build-meta": "Green"}"#;
        let version_data_2 = r#"{"sadf:major": 22,
                                "sadf:minor": 6,
                                "sadf:patch": 12,
                                "sadf:pre-release": "Trial",
                                "sadf:build-meta": "Green"}"#;

        let version_data_3: Version =  Version{ major: 22,
                                                minor: 6,
                                                patch: 12,
                                                pre_release: Some(String::from("Trial")),
                                                build_meta:  Some(String::from("Green"))};

        //from the string form as a value where the keyword is not rusty!!!
        let version_1_value: Value = serde_json::from_str(version_data_1).unwrap();

        //from the string form as a value where the keyword is not rusty and is name spaced!!!
        let version_2_value: Value = serde_json::from_str(version_data_2).unwrap();

        //from the struct form as json
        let version_3_json: Json<Version> = Json(version_data_3);


        assert_eq!(version_1_value["major"], 22);
        assert_eq!(version_1_value["minor"], 6);
        assert_eq!(version_1_value["patch"], 12);
        assert_eq!(version_1_value["pre-release"], "Trial");
        assert_eq!(version_1_value["build-meta"], "Green");

        assert_eq!(version_2_value["sadf:major"], 22);
        assert_eq!(version_2_value["sadf:minor"], 6);
        assert_eq!(version_2_value["sadf:patch"], 12);
        assert_eq!(version_2_value["sadf:pre-release"], "Trial");
        assert_eq!(version_2_value["sadf:build-meta"], "Green");

        assert_eq!(version_3_json.major, 22);
        assert_eq!(version_3_json.minor, 6);
        assert_eq!(version_3_json.patch, 12);
        assert_eq!(version_3_json.pre_release, Some(String::from("Trial")));
        assert_eq!(version_3_json.build_meta, Some(String::from("Green")));

    }
}
