//Copyright SoftwareByNumbers Ltd 2021

//! # Operations on model meta data
//!
//! This module provides a set of operations to manipulate the meta data
//! of a model using the meta data types.  These operations are side effect
//! free with no external service calls.
//!
//! These operations borrow and return cloned objects as required.

use serde_json::Value;

use super::meta_data_types::ModelMetaData;

///error messages
static KEY_NOT_FOUND : &str = "Key not found";


static APP_VERSION_KEY:   &str = "app-version";
static MODEL_VERSION_KEY: &str = "model-version";
static MODEL_META_KEY:    &str = "meta";
static MODEL_URI_KEY:     &str = "uri";

/// Given model meta data, add the uri.  THis is intended for the teams operation where
/// we want to link models together but the models may not contain their own uri.  This makes
/// linking models difficult.
pub fn add_uri_to_meta(meta: &mut ModelMetaData, uri: &str) -> () {

    //NOTE WE ADD IN TWO PLACES BECAUSE THE MODEL IS BROKEN AT THIS POINT!!
    //AFTER FIXING THE DUAL LOCATION IN THE MODEL THIS DOUBLE ADD CAN BE REMOVED.

    meta.uri = Some(String::from(uri));
    meta.detail.uri = Some(String::from(uri));
}

pub fn get_model_meta_uri(model_meta: &Value)-> Result<Value, String> {
    let model_uri: serde_json::Value = model_meta[MODEL_URI_KEY].clone();
    if model_uri.is_null()  {
        Result::Err(String::from(format!("{}: {}", KEY_NOT_FOUND, MODEL_URI_KEY)))
    } else {
        Result::Ok(model_uri)
    }
}


/// Given model meta data in json form, extract and return the model version information.
///
/// # Arguments
///
/// model_meta       - the json representation of a model meta data
///
/// # Return value
///
/// A result with the version information from the model as json.
pub fn get_model_version(model_meta: &Value) -> Result<Value, String> {

    let model_version: serde_json::Value = model_meta[MODEL_VERSION_KEY].clone();
    if model_version.is_null()  {
        Result::Err(String::from(format!("{}: {}", KEY_NOT_FOUND, MODEL_VERSION_KEY)))
    } else {
        Result::Ok(model_version)
    }
}

/// Given model meta data in json form, extract and return the app version information.
///
/// # Arguments
///
/// model_meta       - the json representation of a model meta data
///
/// # Return value
///
/// A result with the version information from the model as json.
pub fn get_app_version(model_meta: &Value) -> Result<Value, String> {
    let app_version = model_meta[APP_VERSION_KEY].clone();
    if app_version.is_null()  {
        Result::Err(String::from(format!("{}: {}", KEY_NOT_FOUND, APP_VERSION_KEY)))
    } else {
        Result::Ok(app_version)
    }
}


/// Given a model in json form, extract and return the meta data information.
///
/// # Arguments
///
/// model       - the json representation of a model
///
/// # Return value
///
/// A result with the meta data information from the model as json.
pub fn get_model_meta(model: &Value) -> Result<Value, String> {
    let model_meta = model[MODEL_META_KEY].clone();
    if model_meta.is_null()  {
        Result::Err(String::from(format!("{}: {}", KEY_NOT_FOUND, MODEL_META_KEY)))
    } else {
        Result::Ok(model_meta)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model_operations::meta_data_types::{Version, Detail};
    use serde_json::Value;

    #[test]
    fn test_model_version() {
        let good_model_meta_data = r#"{"model-version": {"major": 1,
                                                                  "minor": 12,
                                                                  "patch": 22,
                                                                  "pre-release": null,
                                                                  "build-meta": null}
                                        }"#;
        let good_model_version_data = r#"{"major": 1,
                                          "minor": 12,
                                          "patch": 22,
                                          "pre-release": null,
                                          "build-meta": null
                                         }"#;

        let bad_model_meta_data = r#"{"model-version-no": {"major": 1,
                                                           "minor": 12,
                                                           "patch": 22,
                                                           "pre-release": null,
                                                           "build-meta": null}
                                         }"#;

        let good_model_meta: Value    = serde_json::from_str(good_model_meta_data).unwrap();
        let bad_model_meta: Value     = serde_json::from_str(bad_model_meta_data).unwrap();
        let good_model_version: Value = serde_json::from_str(good_model_version_data).unwrap();


        assert_eq!(good_model_meta.is_object(), true);
        assert_eq!(get_model_version(&good_model_meta), Ok(good_model_version));
        assert!(get_model_version(&good_model_meta).unwrap().is_object());
        assert_eq!(get_model_version(&bad_model_meta), Err(String::from("Key not found: model-version")));
    }

    #[test]
    fn test_app_version() {
        let good_app_meta_data = r#"{"app-version": {"major": 110,
                                                     "minor": 12,
                                                     "patch": 22,
                                                     "pre-release": null,
                                                     "build-meta": null}
                                        }"#;
        let good_app_version_data = r#"{"major": 110,
                                        "minor": 12,
                                        "patch": 22,
                                        "pre-release": null,
                                        "build-meta": null
                                        }"#;

        let bad_app_meta_data = r#"{"app-version-no": {"major": 1,
                                                       "minor": 12,
                                                       "patch": 22,
                                                       "pre-release": null,
                                                       "build-meta": null}
                                         }"#;

        let good_app_meta: Value    = serde_json::from_str(good_app_meta_data).unwrap();
        let bad_app_meta: Value     = serde_json::from_str(bad_app_meta_data).unwrap();
        let good_app_version: Value = serde_json::from_str(good_app_version_data).unwrap();


        assert_eq!(good_app_meta.is_object(), true);
        assert_eq!(get_app_version(&good_app_meta), Ok(good_app_version));
        assert!(get_app_version(&good_app_meta).unwrap().is_object());
        assert_eq!(get_app_version(&bad_app_meta), Err(String::from("Key not found: app-version")));
    }
    #[test]
    fn test_add_uri() {
        let version: Version = Version{major: 1,
                                       minor: 22,
                                       patch: 22,
                                       pre_release: None,
                                       build_meta: None};
        let detail: Detail = Detail{ownership: None,
                                    contact: None,
                                    contact_email: None,
                                    uri: None};
        let mut meta: ModelMetaData = ModelMetaData{summary: String::from("Text"),
                                                    app_version: version,
                                                    model_version: None,
                                                    creation_date_time: String::from("22"),
                                                    uri: None,
                                                    uuid: String::from("123"),
                                                    namespace: String::from("qq"),
                                                    board_link: None,
                                                    detail,
                                                    name: String::from("mod")};

        let new_uri = "http://a/api/model/22";
        let some_uri: Option<String> = Some(String::from(new_uri));

        assert_eq!(meta.uri, None);
        add_uri_to_meta(&mut meta, new_uri);
        assert_eq!(meta.uri, some_uri);

    }
}
