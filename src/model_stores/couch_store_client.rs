//Copyright SoftwareByNumbers Ltd 2021

//! Client for the couch db model store.
//!
//! A non-transactional synchronous client for exchanging models with couchdb.
//!
//! Models are all stored under a single db, they need to be stored by realm.
//!
//! TODO Transactions
//! TODO Operations need to take a realm so that models are stored by realm

use http::{HeaderMap, HeaderValue, header::ACCEPT, header::CONTENT_TYPE};
use serde_json::value::Value;
use rocket_contrib::json::Json;
use error_chain::error_chain;

use crate::model_operations::meta_data_types::ModelMetaData;
error_chain! {
    foreign_links {
        Io(std::io::Error);
        HttpRequest(reqwest::Error);
    }
}

/// Temp url of the model store for development
static MODEL_STORE_URL:   &str = "http://192.168.0.12/api/model";
//static MODEL_STORE_URL:   &str = "http://store.sa.softwarebynumbers.com/api/model";
///Temp url of the summary details web service call
static MODEL_STORE_SUMMARY_URL:   &str = "http://192.168.0.12/api/model/summary";
//static MODEL_STORE_SUMMARY_URL:   &str = "http://store.sa.softwarebynumbers.com/api/model/summary";


/// Get all the model uris
/// 
/// # Arguments
/// 
/// # Return value
/// 
/// A result with the model uris as a json string.
pub fn get_model_uris() -> std::result::Result<String, String> {
    let url_json = MODEL_STORE_URL.to_owned();
    let mut headers: http::HeaderMap = HeaderMap::new();
    headers.insert(ACCEPT, HeaderValue::from_str("application/json").unwrap());
    let uris = reqwest::blocking::Client::new().get(&url_json).headers(headers).send();
    println!(">>>>>> {:?}", uris); 
    match uris {
        Ok(r)   =>   match r.status() {
                        reqwest::StatusCode::OK => std::result::Result::Ok(r.text().unwrap()),
                        _                       => {println!(">>>>>> {:?}", r);std::result::Result::Err(String::from("FAILED - MEH"))}},
        Err(e)  =>   std::result::Result::Err(format!("Failed to read uris : {:?}", e))
    }
}

/// Get a specific model by uuid
/// 
/// # Arguments
/// 
/// uuid - an &str of the uuid
/// 
/// # Return value
/// 
/// A result with the model as a json string.
pub fn get_model(uuid: &str) -> std::result::Result<String, String> {
    let mut url: String = MODEL_STORE_URL.to_owned();
    url.push_str("/");
    url.push_str(uuid);
    let mut headers: http::HeaderMap = HeaderMap::new();
    headers.insert(ACCEPT, HeaderValue::from_str("application/json").unwrap());
    let model = reqwest::blocking::Client::new().get(&url).headers(headers).send();
    match model {
        Ok(r)   =>   match r.status() {
                        reqwest::StatusCode::OK => std::result::Result::Ok(r.text().unwrap()),                         
                        _                       => std::result::Result::Err(String::from(r#"{"error": "error"}"#))},
        Err(e)  =>   std::result::Result::Err(format!("Failed to read uris : {:?}", e))
    }
}

/// Get a specific model summary.
/// 
/// # Arguments
/// 
/// uuid - an &str of the model uuid
/// 
/// # Return value
/// 
/// A result with the model summary as a json string.
pub fn get_model_meta(uuid: &str) -> std::result::Result<String, String> {
    let mut url: String = MODEL_STORE_SUMMARY_URL.to_owned();
    url.push_str("/");
    url.push_str(uuid);
    let mut headers: http::HeaderMap = HeaderMap::new();
    headers.insert(ACCEPT, HeaderValue::from_str("application/json").unwrap());
    let summary = reqwest::blocking::Client::new().get(&url).headers(headers).send();
    match summary {
        Ok(r)   =>   match r.status() {
                        reqwest::StatusCode::OK => std::result::Result::Ok(r.text().unwrap()),
                        _                       => std::result::Result::Err(String::from("FAILED - MEH"))},
        Err(e)  =>   std::result::Result::Err(format!("Failed to read uris : {:?}", e))
    }
}

/// Get a specific model summary.
/// 
/// # Arguments
/// 
/// uuid - an &str of the model uuid
/// 
/// # Return value
/// 
/// A result with the model summary as a json string.
pub fn get_model_meta_struct(uuid: &str) -> std::result::Result<Json<ModelMetaData>, String> {
    let mut url: String = MODEL_STORE_SUMMARY_URL.to_owned();
    url.push_str("/");
    url.push_str(uuid);
    let mut headers: http::HeaderMap = HeaderMap::new();
    headers.insert(ACCEPT, HeaderValue::from_str("application/json").unwrap());
    let respon = reqwest::blocking::Client::new().get(&url).headers(headers).send();
    if respon.is_ok() {
        let summary = respon.unwrap();  
        let summary_body: ModelMetaData = summary.json().unwrap();
        std::result::Result::Ok(Json(summary_body))
    }
    else {
        std::result::Result::Err(format!("Failed to read uris"))
    }
}

pub fn get_model_meta_a(uuid: &str) -> std::result::Result<String, String> {
    let mut url: String = MODEL_STORE_SUMMARY_URL.to_owned();
    url.push_str("/");
    url.push_str(uuid);
    match reqwest::blocking::Client::new().get(&url).send() {
        Ok(response)   =>   {
                                if response.status() == reqwest::StatusCode::OK {
                                    match response.text() { 
                                        Ok(text) => std::result::Result::Ok(text),
                                        Err(_)   => std::result::Result::Err(String::from("FAILED - MEH"))
                                    }
                                }
                                else {
                                    std::result::Result::Err(String::from("FAILED - MEH"))

                                }
                            },            
        Err(e)  =>   std::result::Result::Err(format!("Failed to read uris : {:?}", e))
    }
}

/// Update a specific model summary.
/// 
/// # Arguments
/// 
/// uuid - an &str of the model uuid
/// 
/// # Return value
/// 
/// A result with the model summary as a json string.
pub fn update_summary(uuid: &str, updated_summary: Value) -> std::result::Result<String, String> {

    let mut url: String = MODEL_STORE_SUMMARY_URL.to_owned();
    url.push_str("/");
    url.push_str(uuid);
    let mut headers: http::HeaderMap = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_str("application/json").unwrap());
    match reqwest::blocking::Client::new().put(&url).headers(headers).json(&updated_summary).send() {
        Ok(response)   =>   {
            if response.status() == reqwest::StatusCode::OK {
                match response.text() { 
                    Ok(text) => std::result::Result::Ok(text),
                    Err(_)   => std::result::Result::Err(String::from("FAILED - MEH"))
                }
            }
            else {
                std::result::Result::Err(String::from("FAILED - MEH"))

            }
        },            
        Err(e)  =>   std::result::Result::Err(format!("Failed to read uris : {:?}", e))
    }
}

// pub fn update_model_meta_struct(uuid: &str, updated_summary: ModelMetaData) -> std::result::Result<Json<ModelMetaData>, String> {
//     let mut url: String = MODEL_STORE_SUMMARY_URL.to_owned();
//     url.push_str("/");
//     url.push_str(uuid);
//     let mut headers: http::HeaderMap = HeaderMap::new();
//     headers.insert(CONTENT_TYPE, HeaderValue::from_str("application/json").unwrap());
//     match reqwest::blocking::Client::new().put(&url).headers(headers).json(&updated_summary).send() {
//         Ok(response)   =>   {
//             if response.status() == reqwest::StatusCode::OK {
//                 let updated = get_model_meta_struct(uuid);
//                 match updated { 
//                     Ok(text) => updated,
//                     Err(_)   => std::result::Result::Err(String::from("FAILED - MEH"))
//                 }
//             }
//             else {
//                 std::result::Result::Err(String::from("FAILED - MEH"))

//             }
//         },            
//         Err(e)  =>   std::result::Result::Err(format!("Failed to read uris : {:?}", e))
//     }
// }