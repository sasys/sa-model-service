#![feature(proc_macro_hygiene, decl_macro)]

//Copyright SoftwareByNumbers Ltd 2021

//! Model maintenance service.
//!
//! Uses the keycloak security provider for access permissions.
//!
//! Provides
//! A micro service to convert models to markdown and markdown to models.
//!
//! A micro service to check, fix and update model and app version information.
//!
//! Intended to allow model code design to evolve whilst providing
//! a way bring existing models into line with more recent code releases.
//!
//! For this api to operate the client needs to provide a security realm with each request.  This
//! allows the security mechanism to connect to the correct realm for checking security constraints
//! for that client.  The mechanism for establishing the realm is not yet in place so this api uses
//! a configured security realm with a default realm in case the configuration cannot be read.  This
//! is a temporary measure to support testing.
//! Configuration is ead at startup and not refreshed.  The configuration location is fixed in the
//! resources directory.
//!
//! The config.json provides the default realm and the configured security provider.
//!
//! The provider configuration specifies the details of how the logical permissions configuration
//! maps on to the selected provider configuration.
//!
//!
//!
//! TODO The realm needs to come from the client and should be removed from here and from the
//! TODO security configuration.
//!
//!
//! Example default configuration.
//!
//!```json
//! {"security_provider": "keycloak",
//!  "default_realm": "sa-teams"}
//!```
//!
//! Example partial security configuration for keycloak.
//!
//!```json
//!
//! [{"provider": "keycloak",
//!   "realm": "sa-teams",
//!   "token_endpoint": "http://192.168.0.12:8080/auth/realms/REALM/protocol/openid-connect/token",
//!   "services": {"notes": "REST methods for the sa-md service",
//!                "methods": [{"name":          "modeltomd",
//!                             "description":   "Describes the allowed access by the ui client to the json to markdown convertor",
//!                             "resource_name": "json_to_md",
//!                             "common_access": ["Create", "Read"]},
//!                            {"name":           "mdtomodel",
//!                             "description": "Describes the allowed access by the ui client to markdown to json convertor",
//!                             "resource_name": "md_to_json",
//!                             "common_access": ["Create", "Read"]},
//!
//! ...more...
//!```
//!
//!
//!
//!
//!

#[macro_use] extern crate rocket;

extern crate log;
extern crate fern;
extern crate lru;
extern crate chrono;

pub mod model_stores;
pub mod model_operations;
pub mod security;

use serde_json;
use serde_json::value::Value;
use rocket::http::{RawStr, Status};
use rocket::{Data, State, fairing::AdHoc, request, request::{Request, FromRequest}};
use rocket_contrib::json::Json;

use std::{env, io};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Mutex, Arc};

use lru::LruCache;
use log::{info, warn};

use sa_md;

use model_operations::meta_data_operations;
use model_operations::meta_data_types::{ModelMetaData, ModelState, ModelStates};
use model_stores::couch_store_client::{get_model_uris, get_model, get_model_meta,
                                       get_model_meta_struct, update_summary};
use security::config::{read_permissions_mappings, read_general_config,
                       JSON_TO_MARKDOWN_PERMISSIONS, GENERAL_CONFIG};
use security::cache::{get_cache, ClientCacheKey, ClientCacheEntry};
use security::permissions::{permissions, Permissions, AuthToken};
use security::config::{Permission, GeneralConfig};


static LOG_LEVEL_ENV_NAME: &str = "LOG_LEVEL";
static DEFAULT_LOG_LEVEL_ENV: &str = "ERROR";

///The name of the env var we check for the time to live value
static MAX_MODEL_SIZE_ENV_NAME: &str = "MAX_MODEL_SIZE";
///This is used to limit the max size of the model, used to prevent dos attacks.
static DEFAULT_MAX_MODEL_SIZE: &str = "1000000";
///This is used to limit the max size of the model, used to prevent dos attacks.
static DEFAULT_MAX_MODEL_SIZE_U64: u64 = 1000000; //1MB
///This is used to limit the max size of the model, used to prevent dos attacks.
static DEFAULT_MAX_MODEL_SIZE_STR: &str = "1000000";

/// Shortcut name for serde struct
static PROCS: &str = "procs";

///Initial realm until we get the client realm sent through
static REALM: &str = "sa-teams";

///Default time to live for a cache entry
static CACHE_ENTRY_EXPIRES_DELTA: i64 = 60;  //seconds

// Header values from a request guard
// See https://api.rocket.rs/v0.4/rocket/request/trait.FromRequest.html
pub struct ApiKey(String);

//Can the user with the auth token access the service
#[derive(Debug)]
pub struct AuthTokenError;

#[derive(Debug)]
pub enum ApiKeyError {
    InvalidToken,
    Message(String),
    NoProvider,
}

impl<'a, 'r> FromRequest<'a, 'r> for AuthToken {
    type Error = AuthTokenError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let token = request.headers().get_one("Authorization");
        match token {
          Some(token) => {
            request::Outcome::Success(AuthToken(token.to_string()))
          },
          // token does not exist
          None => request::Outcome::Failure((Status::BadRequest, AuthTokenError)),
        }
    }
}


impl<'a, 'r> FromRequest<'a, 'r> for HitCount {
    type Error = ApiKeyError;

    fn from_request(req: &'a Request<'r>) -> request::Outcome<HitCount, ApiKeyError> {
        let hit_count_state = req.guard::<State<HitCount>>().unwrap();
        let current_count = hit_count_state.count.load(Ordering::Relaxed);
        info!("Hit Count: {}", current_count);
        hit_count_state.count.store(current_count + 1, Ordering::Relaxed);
        request::Outcome::Success(HitCount{ count: AtomicUsize::new(current_count + 1)})
    }
}

// Represents a possible response to rest call.
// The plain text response is used for a good json to md conversion.
#[derive(Debug, Responder)]
pub enum SaResponse {
    GeneralError(rocket::response::status::NotFound<String>),
    NoPermission(rocket::response::status::Forbidden<String>),
    Text(String),
    Json(Json<serde_json::Value>)
}



/// Take the complete model and transform it into markdown.
/// The client must provide a valid token.
/// The connection to keycloak will be cached for this operation and a given token.
/// The permissions mappings are read at startup and cached read only
///
/// #Arguments
///
/// sec_cacher is a security cache.
///
/// permissions_mapping is the configuration model for security
///
/// auth_token comes from the client of the service and is the authentication token
/// model is the body of the post.  The model.
///
/// #Return value
///
/// A response
///
#[post("/api/transform/modeltomd", format = "application/json", data = "<model>")]
fn modeltomd(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
             auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model: Data)
               -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients,
                              &permissions_mapping.mappings, "modeltomd",
                             vec!(Permission::Read), &auth_token, &realm.realm,
                                               &config.security_provider);

    if sec_ok.is_ok() && sec_ok.unwrap() {
        let mut model_data: Vec<u8> = vec![];
        let model_result: io::Result<u64> = model.stream_to(&mut model_data);
        match model_result {
            Ok(n) => {info!("read {} bytes from body.", n);
                      let json_model: serde_json::Result<Value> =
                             serde_json::from_str(String::from_utf8(model_data).unwrap().as_str());
                      match json_model {
                            Ok(j)  => SaResponse::Text(sa_md::parse_serde(&j[PROCS])),
                            Err(e) => SaResponse::GeneralError(rocket::response::status::NotFound(
                                        format!("\n\n Error converting message to json_model,
                                                      serde_json error was : {} \n stream to result
                                                      was : {:?}", e, model_result)))

                       }
            },
            Err(e) => SaResponse::GeneralError(rocket::response::status::NotFound(
                        format!("Error streaming model data : {}", e)))
        }

    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}

// Take the complete model as markdown and transform it into json.
#[post("/api/transform/mdtomodel", format = "text/plain", data = "<markdown>")]  //plain
fn mdtomodel<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                 auth_token: AuthToken,realm: State<Realm>, config: State<GeneralConfig>,
                 markdown: Data) -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "mdtomodel",
                             vec!(Permission::Read), &auth_token, &realm.realm,
                             &config.security_provider);
    let mut markdown_data: Vec<u8> = vec![];
    let model_result: io::Result<u64> = markdown.stream_to(&mut markdown_data);

    if sec_ok.is_ok() && sec_ok.unwrap() {
        match model_result {
            Ok(n)  => {info!("read {} bytes from body.", n);
                       let json_model = sa_md::parse_markdown(String::from_utf8(markdown_data).unwrap());
                       SaResponse::Text(json_model)},
            Err(e) => {SaResponse::GeneralError(rocket::response::status::NotFound(format!("\n\n Error streaming model data : {} \n\n ", e)))}
        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}


// Get the model summary for the given id, then extract the app version information.
#[get("/api/app-version/<model_id>")]
fn app_version<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                   auth_token: AuthToken,realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr) -> SaResponse {
    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "app_version",
                             vec!(Permission::Read), &auth_token, &realm.realm,
                             &config.security_provider);

    if sec_ok.is_ok() && sec_ok.unwrap() {

        let meta_response: SaResponse = model_summary(sec_cacher, permissions_mapping, auth_token, realm, config, model_id);
        match meta_response {
            SaResponse::Json(summary_json) => {let app_version = meta_data_operations::get_app_version(&summary_json);
                                               if app_version.is_ok() {
                                                   SaResponse::Json(Json(app_version.unwrap()))
                                               } else {
                                                   SaResponse::GeneralError(rocket::response::status::NotFound(app_version.err().unwrap()))
                                               }

             }
            _                               => {SaResponse::GeneralError(rocket::response::status::NotFound(String::from(format!("Failed to find app version for model {}", model_id))))}

        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}

// Get the model summary for the given id, then extract the model version information.
#[get("/api/model-version/<model_id>")]
fn model_version<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                     auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr) -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "model_version", vec!(Permission::Read), &auth_token, &realm.realm, &config.security_provider);

    if sec_ok.is_ok() && sec_ok.unwrap() {

        let meta_response: SaResponse = model_summary(sec_cacher, permissions_mapping, auth_token, realm, config, model_id);
        match meta_response {
            SaResponse::Json(summary_json) => {let model_version = meta_data_operations::get_model_version(&summary_json);
                                               if model_version.is_ok() {
                                                   SaResponse::Json(Json(model_version.unwrap()))
                                               } else {
                                                   SaResponse::GeneralError(rocket::response::status::NotFound(model_version.err().unwrap()))
                                               }

             }
            _                               => {SaResponse::GeneralError(rocket::response::status::NotFound(String::from(format!("Failed to find model version for model {}", model_id))))}

        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}

//Update the app version of the given model
#[put("/api/model/<model_id>/summary/app-version", format = "application/json", data = "<version>")]
fn update_app_version(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                      auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr, version: Json<String>) -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "update_app_version", vec!(Permission::Read, Permission::Update), &auth_token, &realm.realm, &config.security_provider);

    if sec_ok.is_ok() && sec_ok.unwrap() {

        //get the summary
        let meta_response: SaResponse = model_summary(sec_cacher, permissions_mapping, auth_token, realm, config, model_id);

        match meta_response {
            SaResponse::Json(mut summary_json) => {summary_json["app-version"] = Value::from(version.as_str());
                                               let updated_model  = update_summary(model_id, summary_json.0);
                                               if updated_model.is_ok() {
                                                   let updated_model_s: String = updated_model.unwrap();
                                                   let json_result = serde_json::from_str(updated_model_s.as_str());
                                                   match json_result {
                                                       Ok(r) => SaResponse::Json(Json(r)),
                                                       _     => SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))
                                                   }

                                               } else {
                                                   SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))
                                               }}
            _                  => {SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))}
        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}

//Update the model version of the given model
#[put("/api/model/<model_id>/summary/model-version", format = "json", data = "<version>")]
fn update_model_version(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                        auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr, version: Json<String>) -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "update_model_version",  vec!(Permission::Read, Permission::Update), &auth_token, &realm.realm, &config.security_provider);

    if sec_ok.is_ok() && sec_ok.unwrap() {
        //get the summary
        let meta_response: SaResponse = model_summary(sec_cacher, permissions_mapping, auth_token, realm, config, model_id);
        match meta_response {
            SaResponse::Json(mut summary_json) => {summary_json["model-version"] = Value::from(version.as_str());
                                               let updated_model  = update_summary(model_id, summary_json.0);
                                               if updated_model.is_ok() {
                                                   let updated_model_s: String = updated_model.unwrap();
                                                   let json_result = serde_json::from_str(updated_model_s.as_str());
                                                   match json_result {
                                                       Ok(r) => SaResponse::Json(Json(r)),
                                                       _     => SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))
                                                   }

                                               } else {
                                                   SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))
                                               }}
            _                  => {SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))}
        }


    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from("No permission"))))
    }
}

#[get("/api/model")]
fn model_uris<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                  auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>) -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "model_uris", vec!(Permission::Read), &auth_token, &realm.realm, &config.security_provider);
    if sec_ok.is_ok() && sec_ok.unwrap() {
        let uris = get_model_uris();
        if uris.is_ok() {
            let json_result = serde_json::from_str(&uris.unwrap());
            match json_result {
                Ok(r) => SaResponse::Json(Json(r)),
                _     => SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to get  model uris")))
            }
        } else {
            SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to get uris")))
        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}

#[get("/api/model/<model_id>")]
fn model<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
             auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr) -> SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "model",  vec!(Permission::Read), &auth_token, &realm.realm, &config.security_provider);
    if sec_ok.is_ok() && sec_ok.unwrap() {
        let model = get_model(model_id);
        if model.is_ok() {
            let json_result = serde_json::from_str(&model.unwrap());
            match json_result {
                Ok(r) => SaResponse::Json(Json(r)),
                _     => SaResponse::GeneralError(rocket::response::status::NotFound(String::from(format!("Failed to get  model with id {}", model_id))))
            }
        } else {
            SaResponse::GeneralError(rocket::response::status::NotFound(String::from(format!("Failed to get  model with id {}", model_id))))
        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from(
                     "No permission"))))
    }
}

#[get("/api/model/summary/<model_id>")]
fn model_summary<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                     auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr) -> SaResponse {// Response<'r> {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "model_summary",  vec!(Permission::Read), &auth_token, &realm.realm, &config.security_provider);
    if sec_ok.is_ok() && sec_ok.unwrap() {
        let summary_res = get_model_meta(model_id);
        if summary_res.is_ok() {
            let json_result = serde_json::from_str(&summary_res.unwrap());
            match json_result {
                Ok(r) => SaResponse::Json(Json(r)),
                _     => SaResponse::GeneralError(rocket::response::status::NotFound(String::from(format!("Failed to get  model with id {}", model_id))))
            }
        } else {
            SaResponse::GeneralError(rocket::response::status::NotFound(String::from(format!("Failed to get  model summary with id {}", model_id))))
        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from("No permission"))))
    }
}

#[get("/api/model/<model_id>/summary/uri")]
fn model_uri<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                 auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr) -> Json<Value> {

    let _sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "model_uri",  vec!(Permission::Read), &auth_token, &realm.realm, &config.security_provider);

    let summary_res: Result<String, String> = get_model_meta(model_id);
    if summary_res.is_ok() {
        let summary_string_res = summary_res.unwrap();
        let summary_json: Value<> = serde_json::from_str(summary_string_res.as_str()).unwrap();
        let uri: serde_json::Value = meta_data_operations::get_model_meta_uri(&summary_json).unwrap();
        Json(uri)
    }
    else {
        let error = format!("{{\"status\": \"error\",
                               \"reason\": \"ID {} not found\"}}", model_id);
        Json(serde_json::from_str(error.as_str()).unwrap())
    }
}

//Update the uri of the given model
#[put("/api/model/<model_id>/summary/uri", format = "json", data = "<model_uri>")]
fn update_model_uri(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                    auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>, model_id: &RawStr, model_uri: Json<String>) ->
    SaResponse {

    let sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "update_model_uri",  vec!(Permission::Read, Permission::Update), &auth_token, &realm.realm, &config.security_provider);

    if sec_ok.is_ok() && sec_ok.unwrap() {
        //get the summary
        let meta_response: SaResponse = model_summary(sec_cacher, permissions_mapping, auth_token, realm, config, model_id);


        match meta_response {
            SaResponse::Json(summary_json) => {let mut summary: ModelMetaData = serde_json::from_value(summary_json.into_inner()).unwrap();
                                               meta_data_operations::add_uri_to_meta(&mut summary, model_uri.as_str());

                                               let updated_model  = update_summary(model_id, serde_json::to_value(summary).unwrap());
                                               if updated_model.is_ok() {
                                                   let updated_model_s: String = updated_model.unwrap();
                                                   let json_result = serde_json::from_str(updated_model_s.as_str());
                                                   match json_result {
                                                       Ok(r) => SaResponse::Json(Json(r)),
                                                       _     => SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))
                                                   }

                                               } else {
                                                   SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))
                                               }}
            _                  => {SaResponse::GeneralError(rocket::response::status::NotFound(String::from("Failed to update")))}
        }
    } else {
        SaResponse::NoPermission(rocket::response::status::Forbidden(Some(String::from("No permission"))))
    }

}

// Get a summary of all the model states.
#[get("/api/model/check/status")]
fn model_states<'r>(sec_cacher: State<ClientCache>, permissions_mapping: State<Permissions>,
                    auth_token: AuthToken, realm: State<Realm>, config: State<GeneralConfig>) -> Result<Json<ModelStates>, rocket::response::status::NotFound<String>> {

    let _sec_ok = permissions(&sec_cacher.clients, &permissions_mapping.mappings, "model_states", vec!(Permission::Read), &auth_token, &realm.realm, &config.security_provider);

    //get a list of all the model uris
    let model_uris_res: Result<String, String> = get_model_uris();
    let uris: Vec<String>;
    let mut states: Vec<ModelState> = vec!();
    match model_uris_res {
        Ok(r)  => { let sj: serde_json::Result<Vec<String>> = serde_json::from_str(r.as_str());
                    if sj.is_ok() {
                        let model_uris: Vec<String> = serde_json::from_str(r.as_str()).unwrap();
                        uris = model_uris;
                        let mut state: ModelState = ModelState{has_inner_uri: false,
                                                               has_outer_uri: false,
                                                               has_model_version: false,
                                                               uri:  String::from(""),
                                                               uuid: String::from("")};
                        for uri in uris {
                            //get the summary
                            let uuid = uri.split('/').last();
                            let r = get_model_meta_struct(uuid.unwrap());

                            //build the status struct
                            if r.is_ok() {
                                let s: ModelMetaData = r.unwrap().0;
                                state.has_outer_uri = s.uri.is_some();
                                state.has_inner_uri = s.detail.uri.is_some();
                                state.has_model_version = s.model_version.is_some();
                                state.uuid = s.uuid;
                                state.uri = uri;
                            }
                            //add the struct to the list
                            states.push(ModelState::clone(&state));
                        }
                        Result::Ok(Json(ModelStates{states}))
                    }
                    else {
                        let error = format!("{{\"status\": \"error\",
                                               \"reason\": \"{:?}\"}}", sj.err());
                        Result::Err(rocket::response::status::NotFound(error))

                    }
                  }


        Err(_) => {let error = format!("{{\"status\": \"error\",
                                          \"reason\": \"unknown\"}}");
                   Result::Err(rocket::response::status::NotFound(error))}
    }
}


//fern logger initialisation.  We can only have one logger!!
fn setup_fern_logger(level: &str) -> Result<(), fern::InitError> {
    println!("Setting up logger with level: {}", level);
    let level_filter: log::LevelFilter = match level.to_uppercase().as_str() {
            "TRACE" => log::LevelFilter::Trace,
            "DEBUG" => log::LevelFilter::Debug,
            "INFO"  => log::LevelFilter::Info,
            "WARN"  => log::LevelFilter::Warn,
            "ERROR" => log::LevelFilter::Error,
            _       => {println!("Invalid log level provided, using Error: {}", level);
                        log::LevelFilter::Error}
    };
    println!("level filter: {:?}", level_filter);
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(level_filter)
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}

// Configuration from rocket toml
struct MaxModelSize(u64);

struct HitCount {
    count: AtomicUsize
}

struct Realm {
    realm: String
}

struct ClientCache<'a> {
    clients: Arc<Mutex<LruCache<ClientCacheKey, ClientCacheEntry<'a>>>>
}

fn main() {
    // The permissions maps are read once at startup and cached.  These are read only.
    let permissions_result = read_permissions_mappings(JSON_TO_MARKDOWN_PERMISSIONS);
    let general_config = read_general_config(GENERAL_CONFIG);


    let _provider_name = general_config.as_ref().unwrap().security_provider.clone();
    let default_realm = general_config.as_ref().unwrap().default_realm.clone();

    let r = rocket::ignite();
    let _ = match env::var(LOG_LEVEL_ENV_NAME) {
                Ok(s) if !s.is_empty() => setup_fern_logger(&s).expect("failed to initialize logging."),
                Err(_) | Ok(_)  => {let active_config = r.config();
                                    let extras_config = &active_config.extras;
                                    let log_level = active_config.get_str(&LOG_LEVEL_ENV_NAME.to_lowercase());

                                    match log_level {
                                        Ok(s) => match setup_fern_logger(s) {

                                            Ok(z) => {info!("Logger set up OK {:?}", z);
                                                      info!("Rocket toml active config: {:?}\n", active_config);
                                                      info!("Rocket toml extras: {:?}\n", extras_config);
                                                      info!("Rocket toml log level: {:?}\n", log_level);
                                            },
                                            Err(e) => println!("Failed to set up the logger with level {:?}, {:?}",s, e),
                                        }
                                        Err(_) => match setup_fern_logger(DEFAULT_LOG_LEVEL_ENV) {
                                            Ok(s) => {info!("Logger set up OK with DEFAULT  {:?}", s);
                                                      info!("Rocket toml active config: {:?}\n", active_config);
                                                      info!("Rocket toml extras: {:?}\n", extras_config);
                                                      info!("Rocket toml log level: {:?}\n", log_level);
                                            },
                                            Err(e) => println!("Failed to set up the default logger: {:?}", e),
                                        }
                                    }
                                }
                };

    let error =
        r.attach(AdHoc::on_attach("Assets Config", |rocket| {

                let max_size_r = rocket.config()
                                    .get_str(&MAX_MODEL_SIZE_ENV_NAME.to_lowercase())
                                    .unwrap_or(DEFAULT_MAX_MODEL_SIZE_STR).parse::<u64>();
                match max_size_r {
                    Ok(u)  => {info!("MAX MODEL SIZE FROM ROCKET toml {}", u);
                               Ok(rocket.manage(MaxModelSize(u)))},
                    Err(e) => {warn!("Couldn't read max model size from rocket toml {:?}", e);
                               Ok(rocket.manage(MaxModelSize(DEFAULT_MAX_MODEL_SIZE_U64)))},
                }}))
            .manage(HitCount { count: AtomicUsize::new(0) })
            //security connection cache
            .manage(ClientCache{clients: get_cache()})
            //This until we get the client to send over the actual realm
            .manage(Realm{realm: default_realm})
            //this adds the selected security provider
            .manage(general_config.unwrap())
            //security configuration
            .manage(Permissions{mappings: permissions_result.unwrap()})
            .mount("/", routes![mdtomodel, modeltomd, model_states, update_model_uri, model_uri,
                                app_version, update_app_version, model_version, update_model_version,
                                model_uris, model, model_summary]).launch();

    drop(error);

}
