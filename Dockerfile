FROM rust:slim-buster as build

RUN apt-get update -y
RUN apt-get install pkg-config -y
RUN apt-get -y install libssl-dev

RUN rustup install nightly-2021-05-20

RUN rustup override set nightly-2021-05-20-x86_64-unknown-linux-gnu

# empty shell project
RUN USER=root cargo new --bin sa_model_service
WORKDIR /sa_model_service

# copy over manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# cache dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy source tree
COPY ./src ./src


# build release
RUN rm ./target/release/deps/sa_model_service*
RUN cargo build --release

# final base
FROM rust:slim

# copy the build artifact from the build stage
COPY --from=build /sa_model_service/target/release/sa_model_service .
COPY Rocket.toml .
COPY resources/config.json ./resources/config.json
COPY resources/provider_configuration.json ./resources/provider_configuration.json

# startup command to run binary
CMD ["./sa_model_service"]
