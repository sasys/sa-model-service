# Operations To Manage Model Versions WRT Code Releases

[![CircleCI](https://circleci.com/bb/sasys/sa-model-service.svg?style=svg)](https://circleci.com/bb/sasys/sa-model-service)

This code provides operations in the form of a micro service to find, evaluate and update
model properties to keep those models alligned with specific software versions.     

This is a solution to a problem where the code base advances but models exist which do not
have the latest features that the code base can handle.  For example, some models pre-date the
introduction of cloud icons.  This leads to the updated client showing an icon editor with no
icons.  This micro service is intended to provide the tools to check and update models to a
specific version safely.

The service is written in rust and operates on the standard port 80.  It uses the existing
(Clojure) storage service to access and update stored models.  

The store must be present at ```store.sa.softwarebynumbers.com``` on port 80.  This url should respond (under test conditions)
at


```
/api/model
```

which should return a list in json of all the known models in the store.

In the case of the community edition where no back end is present, this microservice acts in a
stateless way, just applying model transformations as needed.  Thus there are two external
from ends, a staeless and a stateful version.  The stefull version uses the staeless version for
model transformations and the existing sa-store service for the teams edition.

## ToDO

* [] The paths are inconsistent between summary and summary details
* [] Add and check the documentation on each function and rest operation.


Inconsistency in paths:

```
localhost:8000/api/model/summary/123
```
and this
```
localhost:8000/api/model/123/summary/uri
```

## Usage

Run as a service inside docker.

Run stand alone.

## Rust versions

```
rustup show
Default host: x86_64-unknown-linux-gnu
rustup home:  ...

installed toolchains
--------------------

stable-x86_64-unknown-linux-gnu
nightly-x86_64-unknown-linux-gnu (default)

installed targets for active toolchain
--------------------------------------

arm-unknown-linux-gnueabihf
armv7-unknown-linux-gnueabihf
thumbv7em-none-eabi
x86_64-unknown-linux-gnu

active toolchain
----------------

nightly-x86_64-unknown-linux-gnu (default)
rustc 1.50.0-nightly (c919f490b 2020-11-17)

```

## Build

__This needs a nightly__
```
    rustup default nightly
    cargo build
```

## Documentation

Build the project documentation using cargo.  Note that at the time of writing cargo
doesn't seem to process the README.md.  This puts the documentation into target/doc.

```
cargo doc --no-deps
```

## Test

```
    cargo test
```
Or with no capture (to see stdout)
  ```
  cargo test -- --nocapture
  ```

### Testing With Curl

```
cargo run
```

From the project root to update the app version info.

```

    curl -v -X PUT -d @resources/test/app-version-1.json -H 'Content-Type: application/json' localhost:8000/api/model/<model_id>/summary/app-version

    curl -v -X PUT -d @resources/test/model-version-1.json -H 'Content-Type: application/json' localhost:8000/api/model/<model_id>/summary/model-version

```

From the root directory of the md-json project there are a couple of test json files.

```
curl -v -X POST -d @resources/test/test_model_2.json -H "Content-Type: text/plain" localhost/api/transform/modeltomd
```


## Docker Build And Deploy

Note, we need a network here to work with the Clojure service and the CouchDb store.

ALSO __NOTE__ Inside the docker network we cannot see the machines /etc.hosts so add the host to the docker container like:
```
--add-host=sa.softwarebynumbers.com:127.0.0.1
```

```
   docker build -t sa-md .
```

FOR DEBUG PURPOSES:
```
docker run --env LOG_LEVEL=INFO  --name sa-md  -p 8000:8000 --add-host=sa.softwarebynumbers.com:127.0.0.1 --network=sa sa-md
```

## Curl With Security

__Get the app version from the model with the given id__

```rust
curl -H "Authorization: eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKaWg0SFB1R0g4ZlBqb3pmRnJDTTlNMG0tRUtid2JZa0dUNzYwN3NCcTlrIn0.eyJleHAiOjE2MTg0MTgzMjgsImlhdCI6MTYxODQxODAyOCwiYXV0aF90aW1lIjoxNjE4NDE2OTU3LCJqdGkiOiJiOTAyOWM2Yy03ZDZkLTRkZDktYTZjZi1kNzZkM2Q1YjZlOTMiLCJpc3MiOiJodHRwOi8vc2Euc29mdHdhcmVieW51bWJlcnMuY29tOjgwODAvYXV0aC9yZWFsbXMvc2EtdGVhbXMiLCJzdWIiOiJmMzYyYjIxZi00YWMyLTQ1NDAtYmQ4Yi02MDU2MTk2NTFmYWUiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJzYS10ZXN0LXRlYW0tY2xpZW50Iiwibm9uY2UiOiJlZTIzNjM3YS01NjMzLTRjYzEtYWM5ZS1iNTlhMTNjMmY2…maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiYSBiIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYUBiLmNvbSIsImdpdmVuX25hbWUiOiJhIiwiZmFtaWx5X25hbWUiOiJiIiwiZW1haWwiOiJhQGIuY29tIn0.FmXkAYlZktJvG5mlTM3pQbXrE3ttrHwAUWfOZ3HwhE09zP_UxSZCoyqEYoiIwUQ9JNBgCfc21yHdFmtg-OYPSG5Lr3iuuvoTSN-bqTFnMsYnwqBAygkRThsOJ6sJy3ZwVzfsP1W4bbojdIHXQmLBhIfWxWC9oOD63gJcHCtH9FiNHbswQD82T106EF9FZBDuiqBWEO5qn0b2YbY1sXW_mWQlXD7zDnOZCav2-kny_YMhp9FCtF4flSi3yn96KfglxWlTIBoodveCjOYdtd5N_DbsshEKSJV5tuYpZ-BXrse4izv5yi9g_ndmbYjY2mOnftoEBBk5SgBQEdnUwNbo7w" sa.softwarebynumbers.com/api/app-version/23fed73c-0d0a-4ba0-b9a5-b7e1fd4dfcbb
```

__Get the model version from the model with the given id__

```rust
curl -H "Authorization: eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKaWg0SFB1R0g4ZlBqb3pmRnJDTTlNMG0tRUtid2JZa0dUNzYwN3NCcTlrIn0.eyJleHAiOjE2MTg0MTk1MDksImlhdCI6MTYxODQxOTIwOSwiYXV0aF90aW1lIjoxNjE4NDE2OTU3LCJqdGkiOiJhMDVkNmYzNy0yYzM0LTRkZTAtYWM4YS1jNzI0YTU1MmE5ODEiLCJpc3MiOiJodHRwOi8vc2Euc29mdHdhcmVieW51bWJlcnMuY29tOjgwODAvYXV0aC9yZWFsbXMvc2EtdGVhbXMiLCJzdWIiOiJmMzYyYjIxZi00YWMyLTQ1NDAtYmQ4Yi02MDU2MTk2NTFmYWUiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJzYS10ZXN0LXRlYW0tY2xpZW50Iiwibm9uY2UiOiJlZTIzNjM3YS01NjMzLTRjYzEtYWM5ZS1iNTlhMTNjMmY2…maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiYSBiIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYUBiLmNvbSIsImdpdmVuX25hbWUiOiJhIiwiZmFtaWx5X25hbWUiOiJiIiwiZW1haWwiOiJhQGIuY29tIn0.J7CMEVYpmYlWz48vQKpOar9teq8XC47EW41MnOTTxqnaFWG_OHUopBwV1c8FAuP7QDS8NVhq5G5_JDKDvzO9wl3BjLqWOtMab4-JNT5brsLEr3CXFvAcZOvv2udEKNRrSWqWUCTIvWF262BRPyecPhdxgkuOMybrM1XexAFz-W0PQRw9sDYWLu2uqHDrWaE7DHTbQVxdsiay-agqpCjdW1K9-ldLhXplXxEZImIAlOPClPJ1ItUTh3u0zLbsywb2JPzG83alyvQ-RF9LiSeG-pnoTL_2lnKNJ-YtbXFeJh7ClIPPYLTb2PlnhAKg2zQXXAockZB7FzYizPCORGXz4w" sa.softwarebynumbers.com/api/model-version/23fed73c-0d0a-4ba0-b9a5-b7e1fd4dfcbb
```
